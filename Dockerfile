FROM linuxbrew/linuxbrew:latest
MAINTAINER Sugeng Tigefa <sugeng@tigefa.space>


ENV DEBIAN_FRONTEND="teletype" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

RUN brew update
# RUN brew services
# RUN brew bundle
RUN brew install python@2
RUN brew install python3
RUN brew install aria2
RUN brew install curl
RUN brew install wget
RUN brew install whois
RUN brew install netcat
RUN brew install figlet
RUN brew install git
RUN brew install git-lfs
RUN brew install git-flow
RUN brew install git-extras
RUN brew install libgit2
RUN brew install bash-git-prompt
RUN brew install subversion
RUN brew install bazaar
RUN brew install mercurial
RUN brew install node
RUN brew install yarn
RUN brew install ruby
RUN brew install go
RUN brew install perl
RUN brew install v8
RUN brew install ffmpeg
RUN brew install imagemagick
RUN brew install graphicsmagick
RUN brew install maven
RUN brew install ant
RUN brew install jruby
RUN brew install jdk
RUN brew install jemalloc
# RUN brew install gperftools
# RUN sudo apt-get update
# RUN sudo apt-get install -yqq apt-utils
# RUN sudo apt-get install -yqq systemtap-sdt-dev
# RUN sudo apt-get install -yqq zlib1g-dev
RUN brew install automake autoconf curl pcre bison re2c mhash libtool icu4c gettext jpeg openssl libxml2 mcrypt glib gmp libevent
# RUN brew install --only-dependencies php
# RUN brew install composer
RUN brew list
RUN brew cleanup
RUN rm -rf $(brew --cache)
